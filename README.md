# Le magasin général

Le magasin général de Sainte-Anne-des-Plaines recherche un(e) expert(e) en informatique pour développer un système de gestion d'inventaire.

Ce système doit être sécuritaire et nécessite un accès autorisé afin d'interagir avec ce dernier. 

## Requis du magasin général

- Possibilité de créer un item dans l'inventaire (utiliser une base de données en mémoire et non une vraie BD Oracle)
- Afficher un item
- Afficher tous les items en inventaire
- Le système doit être sécurisé à l'aide dun JSON Web Token (JWT)
- Les opérations doivent être testées unitairement pour assurer la qualité
- Le système doit être écrit à l'aide de Spring Boot


### Notes
- Un JWT peut être obtenu à l'aide de l'API suivant:

`curl --request POST 'https://sandbox.account.bellmedia.ca/api/login/v2.1' \
--header 'Authorization: Basic dXNlcm1ndDpkZWZhdWx0' \
--header 'Content-Type: application/x-www-form-urlencoded' \
--data-urlencode 'username=superadmin@magasingeneral.com' \
--data-urlencode 'password=Password1!' \
--data-urlencode 'grant_type=password'`
- Soumettre une merge request et le gestionnaire du magasin général évaluera la soumission

### Bonus
- Créer un pipeline qui valide les tests unitaires
- Valider que le access token utilisé pour accéder au système possède le scope `default`
